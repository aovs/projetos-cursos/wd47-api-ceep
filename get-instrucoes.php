<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    header('Content-type: application/json; charset=UTF-8');
    echo '{"instrucoes":[{"conteudo":"Bem vindo ao Ceep!","cor":"#FFAA10"},{"conteudo":"O site é otimizado para celulares!","cor":"#45AAEE"},{"conteudo":"Para mudar o layout, clique no botão Linha do cabeçalho","cor":"#FF1010"}]}';
    exit;
}