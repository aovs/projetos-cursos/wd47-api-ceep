<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

if ($_SERVER['REQUEST_METHOD'] === 'GET' and isset($_GET['usuario']))
{
    $usuario = filter_var($_GET['usuario'], FILTER_VALIDATE_EMAIL);

    if ($usuario) 
    {
        $json_file_name = __DIR__ . '/db/' . $usuario . '.json';
        $response = array();

        if (file_exists($json_file_name)) {
            $json_data = file_get_contents($json_file_name);
            $response = json_decode($json_data);
        }
        else {
            $response = (object)array(
                'last_update' => '',
                'cartoes' => []
            );
        }

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($response);
        exit;
    }
}