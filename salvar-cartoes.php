<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $json_data = file_get_contents('php://input');
    $user_info = json_decode($json_data);

    if ($user_info->usuario and is_array($user_info->cartoes) and json_last_error() === JSON_ERROR_NONE)
    {
        $json_file_name = __DIR__ . '/db/' . $user_info->usuario . '.json';
        file_put_contents($json_file_name, $json_data);
        header('Content-type: application/json; charset=UTF-8');
        http_response_code(200);

        echo json_encode( array(
                'status' => 1,
                'message' => 'Cartões salvos com sucesso!',
                'quantidade' => count($user_info->cartoes),
                'usuario' => $user_info->usuario
            ) 
        );

        exit;
    }
}